FROM debian:stretch

ENV DEBIAN_FRONTEND noninteractive
ENV GOROOT /usr/local/go
ENV GOPATH "${HOME}/go"
ENV PATH "${GOPATH}/bin:${GOROOT}/bin:${PATH}"

ADD "https://dl.google.com/go/go1.12.4.linux-amd64.tar.gz" go1.12.4.linux-amd64.tar.gz
ADD "https://github.com/bazelbuild/bazel/releases/download/0.25.0/bazel-0.25.0-installer-linux-x86_64.sh" bazel-0.25.0-installer-linux-x86_64.sh
ADD "https://github.com/sabotage-linux/kernel-headers/archive/v3.12.6-6.tar.gz" kernel-headers-v3.12.6-6.tar.gz

COPY packages.sha256 packages.sha256

RUN sha256sum -c packages.sha256 \
  && rm -f packages.sha256 \
  && echo 'Acquire::ForceIPv4 "true";' > /etc/apt/apt.conf.d/99force-ipv4 \
  && apt-get update \
  && apt-get install -y build-essential pkg-config software-properties-common \
     zip zlib1g-dev unzip python curl git jq musl-tools openjdk-8-jdk \
     apt-transport-https ca-certificates gnupg2 libtool cmake automake autoconf \
     ninja-build virtualenv python-pkg-resources python3-pkg-resources \
  && (curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -) \
  && add-apt-repository "deb https://download.docker.com/linux/debian stretch stable" \
  && (curl -fsSL https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -) \
  && apt-add-repository "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch main" \
  && (curl -fsSL "https://deb.nodesource.com/setup_11.x" | bash -) \
  && (curl -fsSL "https://dl.yarnpkg.com/debian/pubkey.gpg" | apt-key add -) \
  && apt-add-repository "deb https://dl.yarnpkg.com/debian/ stable main" \
  && apt-get update \
  && apt-get install -y clang-9 clang-format-9 clang-tidy-9 lld-9 \
     libc++-9-dev libc++abi-9-dev docker-ce docker-ce-cli containerd.io \
     nodejs yarn \
  && tar xzf go1.12.4.linux-amd64.tar.gz -C /usr/local \
  && mkdir -p "${GOPATH}" \
  && chmod +x bazel-0.25.0-installer-linux-x86_64.sh \
  && ./bazel-0.25.0-installer-linux-x86_64.sh \
  && tar xf kernel-headers-v3.12.6-6.tar.gz \
  && make -C kernel-headers-3.12.6-6 CC='musl-gcc' ARCH=x86_64 prefix=/opt DESTDIR=/ install \
  && rm -Rf kernel-headers-3.12.6-6 \
  && rm -f bazel-0.25.0-installer-linux-x86_64.sh go1.12.4.linux-amd64.tar.gz kernel-headers-v3.12.6-6.tar.gz \
  && apt-get clean \
  && rm -Rf /var/lib/apt/lists

# Add additional default include paths (for stripped kernel headers)
COPY musl-gcc.specs /usr/lib/x86_64-linux-musl/musl-gcc.specs
