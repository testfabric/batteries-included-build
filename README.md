# Batteries Included Build

A Docker image based off Debian stretch which includes all the tools needed to build CrashLabsIO's projects. This image is responsible for powering our CI/CD pipelines.

Why a single fixed image for all our pipelines? We use a trunk based model for software development, our build system is no different. This enables us to passively defend against bit rot in our build scripts. All projects are expected to be buildable using the latest stable releases of their corresponding tools.

## Supported Languages

* Golang
* Java (JDK8)
* C (including static linking using musl libc)
* C++
* Node.js

## Additional Tools

* GNU Make
* Bazel Build
* Clang/LLVM
* YARN
* Curl
* Jq

## TODO

* Upgrade to JDK11 when we can get a stable debian package
